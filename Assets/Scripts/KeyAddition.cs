using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyAddition : MonoBehaviour
{
    private KeyAccess keyAmount;
    [SerializeField] GameObject door;
    [SerializeField] AudioSource mainAudioSource;
    [SerializeField] AudioClip keyCardSound;
    // Start is called before the first frame update
    void Start()
    {
        keyAmount = door.GetComponent<KeyAccess>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            mainAudioSource.PlayOneShot(keyCardSound);
            keyAmount.keyHolding += 1;
            Destroy(gameObject);
        }
        
    }
}
