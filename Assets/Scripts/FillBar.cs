using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode()]
public class FillBar : MonoBehaviour
{
    public float maximum;
    public float current;
    public Image mask;
    private float progress;
    private GameObject metric;
    [SerializeField] ObjectiveBehaviour increaseBar;
    // Start is called before the first frame update
    void Start()
    {
        current = 0;
        metric = GameObject.FindWithTag("Pole");
        //float increaseBar = metric.GetComponent<ObjectiveBehaviour>().increaseBar;
        //progress = increaseBar;
    }

    // Update is called once per frame
    void Update()
    {
        GetCurrentFill();
    }
    void GetCurrentFill()
    {
        current = increaseBar.increaseBar;
        float fillAmount = (float)current/ (float)maximum;
        mask.fillAmount = fillAmount;
    }
}
