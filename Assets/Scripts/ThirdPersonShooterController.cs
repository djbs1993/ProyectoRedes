using System.Collections;
using UnityEngine;
using Cinemachine;
using StarterAssets;
using TMPro;
using UnityEngine.SceneManagement;
using Unity.Netcode;

public class ThirdPersonShooterController : NetworkBehaviour
{
    //[SerializeField] private CinemachineVirtualCamera aimVirtualCamera;
    [SerializeField] private float aimSensitivity;
    [SerializeField] private float normalSensitivity;
    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask();
    [SerializeField] private Transform pfBulletProjectile;
    [SerializeField] private Transform spawnBulletPosition;
    [SerializeField] private Transform SecondspawnBulletPosition;
    [SerializeField] private Transform ThirdspawnBulletPosition;
    [SerializeField] private Transform trapBomb;
    [SerializeField] private GameObject StartMenu;
    [SerializeField] private GameObject redDot;
    [SerializeField] private GameObject GameManager;
    [SerializeField] private GameObject pistol;
    [SerializeField] private GameObject shotgun;
    [SerializeField] private GameObject drawingPistol;
    [SerializeField] private GameObject drawingShotgun;
    [SerializeField] private AudioSource mainAudioSource;
    [SerializeField] private TMP_Text TrapsNumber;
    [SerializeField] private TMP_Text bullets;
    [SerializeField] private TMP_Text topBullets;
    [SerializeField] private TMP_Text SecondtopBullets;
    [SerializeField] private TMP_Text secondBullets;
    public CameraShake cameraShake;
    public CameraShake secondCameraShake;

    private ThirdPersonController thirdPersonController;
    private StarterAssetsInputs starterAssetsInputs;
    private GameManager health;
    [SerializeField] private AudioClip blasterClip;
    [SerializeField] private AudioClip heavyBlasterClip;
    [SerializeField] private AudioClip landMinePlacementClip;
    private Trap trap;
    private Animator animator;
    private bool hasRecievedDamageRecently = false;
    private bool pause;
    private bool firstWeapon = false;
    private bool shooting = false;

    public int trapAmount = 3;
    public int munition = 36;
    public int secondMunition = 10;
    private Scene currentScene;
    private string sceneName;

    private void Awake()
    {
        // Find objects in the scene
        //aimVirtualCamera = GameObject.Find("PlayerAimCamera")?.GetComponent<CinemachineVirtualCamera>();

        //trapBomb = Resources.Load<Transform>("Prefabs/TrapBomb");
        StartMenu = GameObject.FindGameObjectWithTag("StartMenu");
        redDot = GameObject.FindGameObjectWithTag("RedDot");
        GameManager = GameObject.FindGameObjectWithTag("GameManager");
        //pistol = GameObject.FindGameObjectWithTag("Pistol");
        //shotgun = GameObject.FindGameObjectWithTag("Shotgun");

        mainAudioSource = FindObjectOfType<AudioSource>();
        TrapsNumber = GameObject.Find("NumberOfTraps")?.GetComponent<TMP_Text>();
        bullets = GameObject.Find("AmountBullets")?.GetComponent<TMP_Text>();
        topBullets = GameObject.Find("TopAmountBullets")?.GetComponent<TMP_Text>();
        SecondtopBullets = GameObject.Find("2ndTopAmountBullets")?.GetComponent<TMP_Text>();
        secondBullets = GameObject.Find("2ndAmountBullets")?.GetComponent<TMP_Text>();

        // Initialize components
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
        firstWeapon = true;

        cameraShake = FindObjectOfType<CameraShake>();
        secondCameraShake = FindObjectOfType<CameraShake>();
        health = GameManager.GetComponent<GameManager>();
        thirdPersonController = GetComponent<ThirdPersonController>();
        starterAssetsInputs = GetComponent<StarterAssetsInputs>();
        trap = GetComponent<Trap>();
        animator = GetComponent<Animator>();
        StartMenu.SetActive(false);
        drawingShotgun.SetActive(false);
        munition = PlayerPrefs.GetInt("munitionSaved", 36);
        secondMunition = PlayerPrefs.GetInt("secondMunitionSaved", 10);
        trapAmount = PlayerPrefs.GetInt("trapAmountSaved", 3);
        health.health = PlayerPrefs.GetInt("healthAmountSaved", 4);
    }

    private void Start()
    {
        FirstLevel();
    }

    void Update()
    {
        if (!IsOwner)
        {
            return;
        }

        munition = Mathf.Clamp(munition, 0, 99);
        secondMunition = Mathf.Clamp(secondMunition, 0, 30);


        if (firstWeapon == true)
        {

            bullets.gameObject.SetActive(true);
            topBullets.gameObject.SetActive(true);
            secondBullets.gameObject.SetActive(false);
            SecondtopBullets.gameObject.SetActive(false);
        }

        if (firstWeapon == false)
        {

            bullets.gameObject.SetActive(false);
            topBullets.gameObject.SetActive(false);
            secondBullets.gameObject.SetActive(true);
            SecondtopBullets.gameObject.SetActive(true);
        }

        TrapsNumber.text = trapAmount.ToString();
        bullets.text = munition.ToString();
        secondBullets.text = secondMunition.ToString();

        Vector3 mouseWorldPosition = Vector3.zero;
        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimColliderLayerMask))
        {
            mouseWorldPosition = raycastHit.point;
        }

        if (starterAssetsInputs.aim && !pause)
        {
            shooting = true;
            //aimVirtualCamera.gameObject.SetActive(true);
            thirdPersonController.SetSensitivity(aimSensitivity);
            thirdPersonController.SetRotationOnMove(false);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 1f, Time.deltaTime * 10f));

            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;

            transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20f);
        }
        else
        {
            shooting = false;
            //aimVirtualCamera.gameObject.SetActive(false);
            thirdPersonController.SetSensitivity(normalSensitivity);
            thirdPersonController.SetRotationOnMove(true);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 0f, Time.deltaTime * 10f));
        }

        if (starterAssetsInputs.shoot && !pause && munition > 0 && firstWeapon == true && shooting == true)
        {
            munition -= 1;
            Vector3 aimDir = (mouseWorldPosition - spawnBulletPosition.position).normalized;
            mainAudioSource.PlayOneShot(blasterClip);
            Instantiate(pfBulletProjectile, spawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
            starterAssetsInputs.shoot = false;
        }

        if (starterAssetsInputs.shoot && !pause && secondMunition > 0 && firstWeapon == false && shooting == true)
        {
            secondMunition -= 1;
            Vector3 aimDir = (mouseWorldPosition - spawnBulletPosition.position).normalized;
            mainAudioSource.PlayOneShot(heavyBlasterClip);
            Instantiate(pfBulletProjectile, spawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
            Instantiate(pfBulletProjectile, SecondspawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
            Instantiate(pfBulletProjectile, ThirdspawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
            starterAssetsInputs.shoot = false;
        }

        if (starterAssetsInputs.changeWeapon && firstWeapon == true)
        {
            drawingPistol.gameObject.SetActive(false);
            drawingShotgun.gameObject.SetActive(true);
            firstWeapon = false;
            pistol.gameObject.SetActive(false);
            shotgun.gameObject.SetActive(true);
            starterAssetsInputs.changeWeapon = false;
        }

        if (starterAssetsInputs.changeWeapon && firstWeapon == false)
        {
            drawingPistol.gameObject.SetActive(true);
            drawingShotgun.gameObject.SetActive(false);
            pistol.gameObject.SetActive(true);
            shotgun.gameObject.SetActive(false);
            firstWeapon = true;
            starterAssetsInputs.changeWeapon = false;
        }

        if (starterAssetsInputs.trap && !pause && trapAmount > 0)
        {
            starterAssetsInputs.trap = false;
            mainAudioSource.PlayOneShot(landMinePlacementClip);
            trapAmount -= 1;
            Instantiate(trapBomb, transform.position, trapBomb.transform.rotation);
        }

        if (starterAssetsInputs.pause)
        {
            if (!pause)
            {
                OnPauseState();
            }
            else
            {
                OutOfPauseState();
            }
        }

        PlayerPrefs.SetInt("munitionSaved", munition);
        PlayerPrefs.SetInt("secondMunitionSaved", secondMunition);
        PlayerPrefs.SetInt("trapAmountSaved", trapAmount);
        PlayerPrefs.SetInt("healthAmountSaved", health.health);
    }

    
    IEnumerator WaitForSecondsToInstantiate()
    {
        hasRecievedDamageRecently = true;
        cameraShake.ShakeCamera(1f, 0.2f);
        secondCameraShake.ShakeCamera(.5f, 0.2f);
        health.health -= 1;
        yield return new WaitForSeconds(1f);
        hasRecievedDamageRecently = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!IsOwner)
        {
            if (!hasRecievedDamageRecently && other.gameObject.CompareTag("enemy"))
            {
                StartCoroutine(WaitForSecondsToInstantiate());
            }
        }
        
    }

    public void OnPauseState()
    {
        pause = true;
        starterAssetsInputs.pause = false;
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        StartMenu.SetActive(true);
        redDot.SetActive(false);
        drawingPistol.SetActive(false);
        drawingShotgun.SetActive(false);
        TrapsNumber.gameObject.SetActive(false);
    }

    public void OutOfPauseState()
    {
        pause = false;
        starterAssetsInputs.pause = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        StartMenu.SetActive(false);
        redDot.SetActive(true);
        drawingPistol.SetActive(true);
        drawingShotgun.SetActive(true);
        TrapsNumber.gameObject.SetActive(true);
    }

    public void OutOfApp()
    {
        Application.Quit();
    }

    public void ReturnToHomeScreen()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void FirstLevel()
    {
        if (sceneName == "Playground")
        {
            trapAmount = 3;
            munition = 36;
            secondMunition = 10;
            health.health = 4;
            PlayerPrefs.DeleteKey("munitionSaved");
            PlayerPrefs.DeleteKey("secondMunitionSaved");
            PlayerPrefs.DeleteKey("trapAmountSaved");
            PlayerPrefs.DeleteKey("healthAmountSaved");
        }
    }
}
