using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;
using UnityEngine.UI;
using TMPro;

public class Munition : MonoBehaviour
{
    
    private StarterAssetsInputs shootInput;

    public int munition;

    private void Start()
    {
        shootInput = GetComponent<StarterAssetsInputs>();
        munition = 36;
    }

    private void Update()
    {

        if (shootInput.shoot)
        {
            munition -= 1;
        }

        if (munition <= 0)
        {
            munition = 0;
        }

        //bullets.text = munition.ToString();

        
    }
}
