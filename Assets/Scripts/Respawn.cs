using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{

    [SerializeField] GameObject SpawnPoint;
    public Vector3 spawnPoint;
    private GameManager health;
    [SerializeField] GameObject GameManager;
    private CharacterController player;
    [SerializeField] CharacterController characterController;
    private bool playerSpawned;
    // Start is called before the first frame update
    void Start()
    {
        playerSpawned = false;
        if (player != null)
        {
            playerSpawned = true;
            player = FindObjectOfType<CharacterController>();
            health = GameManager.GetComponent<GameManager>();
            characterController = player.GetComponent<CharacterController>();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null && playerSpawned == false)
        {
            Start();
        }
        
        
    }

    private void OnTriggerEnter(Collider other)
    {
            if (other.gameObject.tag == "Player")
        {
            characterController.gameObject.SetActive(false);
            other.transform.position = new Vector3(3.32f, 0, 0);
            health.health -= 1;
            characterController.gameObject.SetActive(true);
        }
            
        
    }
}
