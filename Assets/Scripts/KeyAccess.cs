using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KeyAccess : MonoBehaviour
{

    public int keyHolding;
    [SerializeField] Transform door;
    [SerializeField] private float speed = 10f;
    [SerializeField] Transform top;
    [SerializeField] GameObject inputCommand;
    [SerializeField] Transform objective;
    [SerializeField] GameObject finalBoss;
    [SerializeField] AudioSource mainAudioSource;
    [SerializeField] AudioClip doorMoving;
    [SerializeField] AudioClip enemyDetected;
    public bool moving;
    // Start is called before the first frame update
    void Start()
    {
        finalBoss.SetActive(false);
        inputCommand.gameObject.SetActive(false);
        //objective.Vector3.SetActive(true);
        keyHolding = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            
            transform.position = Vector3.MoveTowards(transform.position, objective.position, speed * Time.deltaTime);
            finalBoss.SetActive(true);
            inputCommand.gameObject.SetActive(false);
        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        
        if (other.gameObject.tag=="Player")
        {
            inputCommand.gameObject.SetActive(true);
            if (Input.GetKey(KeyCode.E) && keyHolding >= 2 && !moving)
            {
                mainAudioSource.PlayOneShot(doorMoving);
                mainAudioSource.PlayOneShot(enemyDetected);
                moving = true;
            }

            

            if (keyHolding < 2)
            {
                inputCommand.gameObject.SetActive(false);

            }
            
        }
    }
}
