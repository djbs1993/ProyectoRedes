using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour
{
    private GameManager health;
    private Munition bullets;
    private GameObject player;
    private bool playerHasSpawned;
    [SerializeField] GameObject retryMenu;
    
    [SerializeField] GameObject GameManager;
    [SerializeField] ThirdPersonShooterController thirdPersonShooterController;
    


    private void Start()
    {
        playerHasSpawned = false;
        if (player != null && playerHasSpawned == true)
        {
            playerHasSpawned = false;
            thirdPersonShooterController = FindObjectOfType<ThirdPersonShooterController>();
            health = GameManager.GetComponent<GameManager>();
            bullets = thirdPersonShooterController.GetComponent<Munition>();
        }
        
    }

    private void Update()
    {
        if (player = null)
        {
            playerHasSpawned = true;
            Start();
        }
        //thirdPersonShooterController = FindObjectOfType<ThirdPersonShooterController>();
        //health = GameManager.GetComponent<GameManager>();
        //bullets = thirdPersonShooterController.GetComponent<Munition>();
    }
    public void RestartLevel()
    {
        PlayerPrefs.SetInt("munitionSaved", 36);
        Time.timeScale = 1;
        health.health = 4;
        bullets.munition = 36;
        retryMenu.SetActive(false);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
