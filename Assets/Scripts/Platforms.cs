using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platforms : MonoBehaviour
{
    [SerializeField] Transform[] target;
    
    [SerializeField] AudioSource platformAudioSource;
    [SerializeField] AudioClip movingPlatform;
    private float speed = 3f;

    int currentPosition = 0;
    int nextPosition = 1;

    bool moveNext = true;
    [SerializeField] float timeToNext = 2f;

    private void FixedUpdate()
    {
        if (moveNext)
        {
            transform.position = Vector3.MoveTowards(transform.position, target[nextPosition].position, speed * Time.deltaTime);
        }
        

        if (Vector3.Distance(transform.position, target[nextPosition].position) <= 0)
        {
            StartCoroutine(TimeToMove());
            currentPosition = nextPosition;
            nextPosition++;

            if (nextPosition > target.Length - 1)
            {
                nextPosition = 0;
            }
        }
    }

    IEnumerator TimeToMove()
    {
        moveNext = false;
        yield return new WaitForSeconds(timeToNext);
        moveNext = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            platformAudioSource.PlayOneShot(movingPlatform);
            other.transform.SetParent(transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        platformAudioSource.Stop();
        other.transform.SetParent(null);
    }
}
