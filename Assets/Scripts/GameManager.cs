using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;


public class GameManager : MonoBehaviour
{

    [SerializeField] GameObject gameOver;
    [SerializeField] GameObject heart0;
    [SerializeField] GameObject heart1;
    [SerializeField] GameObject heart2;
    [SerializeField] GameObject heart3;
    [SerializeField] GameObject heart4;
    [SerializeField] GameObject restartButton;
    [SerializeField] GameObject trapsLabel;
    [SerializeField] GameObject bulletLabel;
    [SerializeField] GameObject munitionPrefab;
    [SerializeField] GameObject healthCratePrefab;
    
    public int health = 4;
    private float targetTime = 25f;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        
        heart0.gameObject.SetActive(true);
        heart1.gameObject.SetActive(true);
        heart2.gameObject.SetActive(true);
        heart3.gameObject.SetActive(true);
        heart4.gameObject.SetActive(true);
        gameOver.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(false);
        trapsLabel.gameObject.SetActive(true);
        bulletLabel.gameObject.SetActive(true);
        
    }

    // Update is called once per frame
    void Update()
    {
        targetTime -= Time.deltaTime;
        heart4.gameObject.SetActive(health >= 4);
        heart3.gameObject.SetActive(health >= 3);
        heart2.gameObject.SetActive(health >= 2);
        heart1.gameObject.SetActive(health >= 1);


        if (health <= 0)
        {
            gameOver.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0;
        }

        if (targetTime <= 0)
        {
            SpawnMunition();
            SpawnHealthCrate();
        }
        //switch (health)
        //{
        //    case 4:
        //        //heart0.gameObject.SetActive(true);
        //        heart1.gameObject.SetActive(true);
        //        heart2.gameObject.SetActive(true);
        //        heart3.gameObject.SetActive(true);
        //        heart4.gameObject.SetActive(true);
        //        break;
        //    case 3:
        //        //heart0.gameObject.SetActive(true);
        //        heart1.gameObject.SetActive(true);
        //        heart2.gameObject.SetActive(true);
        //        heart3.gameObject.SetActive(true);
        //        heart4.gameObject.SetActive(/*false*/true);
        //        break;
        //    case 2:
        //        //heart0.gameObject.SetActive(true);
        //        heart1.gameObject.SetActive(true);
        //        heart2.gameObject.SetActive(true);
        //        heart3.gameObject.SetActive(/*false*/true);
        //        heart4.gameObject.SetActive(false);
        //        break;
        //    case 1:
        //        //heart0.gameObject.SetActive(true);
        //        heart1.gameObject.SetActive(true);
        //        heart2.gameObject.SetActive(/*false*/true);
        //        heart3.gameObject.SetActive(false);
        //        heart4.gameObject.SetActive(false);
        //        break;
        //    case 0:
        //        //heart0.gameObject.SetActive(true);
        //        heart1.gameObject.SetActive(/*false*/true);
        //        heart2.gameObject.SetActive(false);
        //        heart3.gameObject.SetActive(false);
        //        heart4.gameObject.SetActive(false);
        //        break;
        //    default:
        //        //heart0.gameObject.SetActive(false);
        //        heart1.gameObject.SetActive(false);
        //        heart2.gameObject.SetActive(false);
        //        heart3.gameObject.SetActive(false);
        //        heart4.gameObject.SetActive(false);
        //        gameOver.gameObject.SetActive(true);
        //        restartButton.gameObject.SetActive(true);
        //        Cursor.lockState = CursorLockMode.None;
        //        Cursor.visible = true;
        //        Time.timeScale = 0;
        //        break;
        //}
    }

    private void SpawnMunition()
    {
        Instantiate(munitionPrefab, FindObjectOfType<ThirdPersonShooterController>().transform.position + (Vector3.forward * 2) + (Vector3.up *1), munitionPrefab.transform.rotation);
        targetTime = 25f;
    }
    
    private void SpawnHealthCrate()
    {
        Instantiate(healthCratePrefab, FindObjectOfType<ThirdPersonShooterController>().transform.position + (Vector3.forward * 4) + (Vector3.up * 1), healthCratePrefab.transform.rotation);
    }
}
