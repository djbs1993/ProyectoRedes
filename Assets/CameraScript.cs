using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using Cinemachine;

public class CameraScript : NetworkBehaviour
{
    [SerializeField] GameObject mainCameraHost;
    //[SerializeField] GameObject aimCameraHost;
    [SerializeField] GameObject followCameraHost;
    [SerializeField] GameObject mainCameraClient;
    //[SerializeField] GameObject aimCameraClient;
    [SerializeField] GameObject followCameraClient;

    private void Start()
    {
 
    }

    public override void OnNetworkSpawn()
    {
        if (IsHost)
        {
            
            if (IsOwner)
            {
                //followCameraHost.Priority = 20;
                //followCameraClient.Priority = 0;
                mainCameraHost.SetActive(true);
                //aimCameraHost.SetActive(true);
                followCameraHost.SetActive(true);
                mainCameraClient.SetActive(false);
                //aimCameraClient.SetActive(false);
                followCameraClient.SetActive(false);
            }

            else
            {
                //followCameraHost.Priority = 0;
                //followCameraClient.Priority = 20;
                mainCameraHost.SetActive(false);
                //aimCameraHost.SetActive(false);
                followCameraHost.SetActive(false);
                mainCameraClient.SetActive(false);
                //aimCameraClient.SetActive(false);
                followCameraClient.SetActive(false);
            }
        }

        else
        {
            
            if (IsOwner)
            {
                //followCameraClient.Priority = 20;
                //followCameraHost.Priority = 0;
                mainCameraHost.SetActive(false);
                //aimCameraHost.SetActive(false);
                followCameraHost.SetActive(false);
                mainCameraClient.SetActive(true);
                //aimCameraClient.SetActive(true);
                followCameraClient.SetActive(true);
            }

            else
            {
                //followCameraHost.Priority = 20;
                //followCameraClient.Priority = 0;
                mainCameraHost.SetActive(false);
                //aimCameraHost.SetActive(false);
                followCameraHost.SetActive(false);
                mainCameraClient.SetActive(false);
                //aimCameraClient.SetActive(false);
                followCameraClient.SetActive(false);
            }
        }

        }
    // Start is called before the first frame update

}
