using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerFollowCam : MonoBehaviour
{
    private CinemachineVirtualCamera playerFollowCamera;
    private ThirdPersonShooterController playerPrefab;
    private GameObject playerCameraRoot;
    // Start is called before the first frame update
    void Start()
    {
        playerFollowCamera = GetComponent<CinemachineVirtualCamera>();
        playerPrefab = FindObjectOfType<ThirdPersonShooterController>();
        //playerFollowCamera.Follow = playerPrefab.PlayerCameraRoot;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
